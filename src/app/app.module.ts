import { UserUpdateFormComponent } from './users/user-update-form/user-update-form.component';
import { UserFormComponent } from './users/user-form/user-form.component';
import { UsersService } from './users/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { environment } from './../environments/environment';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FireUsersComponent } from './fire-users/fire-users.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent, 
    NavigationComponent,
    UserFormComponent,
    UserUpdateFormComponent,
    FireUsersComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule, 
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'', component:UsersComponent}, //default - localhost:4200 - homepage
      {path:'products', component:ProductsComponent}, //localhost:4200/products
      {path:'user-update-form/:id', component:UserUpdateFormComponent},
      {path:'fireusers', component:FireUsersComponent},
      {path:'**', component:NotFoundComponent} //all the routs that donwt exist
    ])
  ],
  providers: [
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
