import { UsersService } from './../users.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-update-form',
  templateUrl: './user-update-form.component.html',
  styleUrls: ['./user-update-form.component.css']
})
export class UserUpdateFormComponent implements OnInit {

  @Output() addUser:EventEmitter<any> = new EventEmitter<any>(); //any is the type. could be [] or {}
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>(); //pessimistic

  user;
  service:UsersService;
  
  userform = new FormGroup({
    name:new FormControl(),
    phone:new FormControl(),
  });
  
  constructor(private route: ActivatedRoute ,service: UsersService, private router: Router) {
    this.service = service;
  }

  sendData() { //הפליטה של העדכון לאב
    this.addUser.emit(this.userform.value.name);
    console.log(this.userform.value);
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putUser(this.userform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.addUserPs.emit();
          this.router.navigateByUrl("/");//return to main page
        }
      );
    })
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
    })
  }

}

  

