import { UsersService } from './../users.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})

export class UserFormComponent implements OnInit {

  @Output() addUser: EventEmitter<any> = new EventEmitter<any>();

  @Output() addUserPs: EventEmitter<any> = new EventEmitter<any>();
  
  service:UsersService;
  
  userform = new FormGroup({
    name: new FormControl(),
    phone: new FormControl()
  });

  sendData(){
    this.addUser.emit(this.userform.value.name);
    this.service.postUser(this.userform.value).subscribe(response =>{
      this.addUserPs.emit();
      console.log(response);
    });
  }

  constructor(service:UsersService) { 
    this.service = service;
  }

  ngOnInit() {
  }

}





